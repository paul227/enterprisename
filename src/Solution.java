import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Solution {

    public static String solution(String names, String company) {

        return Stream.of(normalizeNames(names))
                .map(name -> Employee.of(name, company))
                .map(e -> e.getIdEmployee(e))
                .collect(Collectors.joining(", "));
    }

    private static String[] normalizeNames(String names) {
        return names.split(",");
    }
}
