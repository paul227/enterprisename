import java.text.MessageFormat;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

record Name(String firstName,
            String middleName,
            String lastName){}

public record Employee (Name parsedName,
                        String companyName,
                        String email) {
    private static final int MAX_LENGTH_LAST_NAME = 8;
    private static HashMap<String, Integer> emailCountMap = new HashMap<>();
    private static HashMap<Employee, String> nameMap = new HashMap<>();

    public  static Employee of(String name, String companyName) {
        Name parsedName = parseName(name);

        String email = maybeReformatEmail(getCompanyEmail(parsedName,
                                                          companyName.toLowerCase()));

        Employee employee = new Employee(parsedName, companyName, email);
        nameMap.put(employee, name);

        return employee;
    }

    public String getIdEmployee(Employee e) {
        return String.format("%s<%s>", nameMap.get(e), email);
    }

    private static String getCompanyEmail(Name parsedName,
                                          String companyName) {
        String firstNameFirstLetter = parsedName.firstName().toLowerCase().substring(0, 1);
        String truncatedLastName = truncateLastName(parsedName.lastName()).toLowerCase();
        String controlFormat = !parsedName.middleName().isEmpty() ? "{0}_{1}_{2}@{3}.com" : "{0}_{1}@{2}.com";
        String[] args = !parsedName.middleName().isEmpty() ? new String[]{
            firstNameFirstLetter,
            parsedName.middleName().toLowerCase().substring(0, 1),
            truncatedLastName,
            companyName
        } : new String[]{
            firstNameFirstLetter,
            truncatedLastName,
            companyName
        };

        return MessageFormat.format(controlFormat, (Object[]) args);
    }

    private static String truncateLastName(String lastName) {
        String normalizedLastName = (lastName.contains("-") ? String.join("",
                                                                          lastName.split("-")) : lastName)
            .replaceAll(" ", "");

        return normalizedLastName.length() > MAX_LENGTH_LAST_NAME ? normalizedLastName.substring(0, MAX_LENGTH_LAST_NAME)
            : normalizedLastName;
    }

    private static String reformatEmailWithNumber(String email, int number) {
        if (number <= 1) {
            return email;
        }
        String[] emailParts = email.split("@");
        return String.format("%s%d@%s", emailParts[0], number, emailParts[1]);
    }

    private static String maybeReformatEmail(String email) {
        Integer count = emailCountMap.merge(email, 1, Integer::sum);
        return reformatEmailWithNumber(email, count);
    }

    private static Name parseName(String name) {
        String[] nameParts = name.trim().split(" ");
        List<String> namePartsList = Arrays.asList(nameParts);
        if (nameParts.length > 2 ) {
            return new Name(nameParts[0], nameParts[1], String.join(" ", namePartsList.subList(2, namePartsList.size())));
        } else {
            return new Name(nameParts[0], "", nameParts[1]);
        }
    }
}
